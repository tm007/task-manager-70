package ru.tsc.apozdnov.tm.api.service.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDtoModel> {

}