package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

import java.util.List;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDtoModel> {

    @NotNull
    List<ProjectDtoModel> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDtoModel findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);


}