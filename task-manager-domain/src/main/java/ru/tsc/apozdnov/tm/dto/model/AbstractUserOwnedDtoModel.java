package ru.tsc.apozdnov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.model.IWBS;
import ru.tsc.apozdnov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoModel extends AbstractDtoModel implements IWBS {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    private Date dateEnd;

    public AbstractUserOwnedDtoModel(
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin
    ) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + "; " + description + "; " + Status.toName(status);
    }

}